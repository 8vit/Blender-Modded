/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The Original Code is Copyright (C) 2013 Blender Foundation.
 * All rights reserved.
 */

/** \file ghost/intern/GHOST_ContextCGL.mm
 *  \ingroup GHOST
 *
 * Definition of GHOST_ContextCGL class.
 */

#include "GHOST_ContextCGL.h"

#include <Cocoa/Cocoa.h>
#include <QuartzCore/QuartzCore.h>
#include <Metal/Metal.h>

//#define GHOST_MULTITHREADED_OPENGL

#ifdef GHOST_MULTITHREADED_OPENGL
#  include <OpenGL/OpenGL.h>
#endif

#include <vector>
#include <cassert>

namespace {
constexpr MTLPixelFormat framebufferMetalPixelFormat = MTLPixelFormatBGRA8Unorm;

constexpr OSType cvPixelFormat = kCVPixelFormatType_32BGRA;
}

NSOpenGLContext *GHOST_ContextCGL::s_sharedOpenGLContext = nil;
int GHOST_ContextCGL::s_sharedCount = 0;

GHOST_ContextCGL::GHOST_ContextCGL(bool stereoVisual,
                                   NSView *view,
                                   CAMetalLayer *metalLayer,
                                   int contextProfileMask,
                                   int contextMajorVersion,
                                   int contextMinorVersion,
                                   int contextFlags,
                                   int contextResetNotificationStrategy)
    : GHOST_Context(stereoVisual),
      m_view(view),
      m_metalLayer(metalLayer),
      m_metalCmdQueue(nil),
      m_metalRenderPipeline(nil),
      m_openGLContext(nil),
      m_defaultFramebuffer(0),
      m_defaultFramebufferMetalTexture(nil),
      m_debug(contextFlags)
{
  // for now be very strict about OpenGL version requested
  switch (contextMajorVersion) {
    case 2:
      assert(contextMinorVersion == 1);
      assert(contextProfileMask == 0);
      m_coreProfile = false;
      break;
    case 3:
      // Apple didn't implement 3.0 or 3.1
      assert(contextMinorVersion == 3);
      assert(contextProfileMask == GL_CONTEXT_CORE_PROFILE_BIT);
      m_coreProfile = true;
      break;
    default:
      assert(false);
  }

  if (m_metalLayer) {
    @autoreleasepool {
      id<MTLDevice> device = m_metalLayer.device;

      // Create a command queue for blit/present operation
      m_metalCmdQueue = (MTLCommandQueue *)[device newCommandQueue];
      [m_metalCmdQueue retain];

      // Create shaders for blit operation
      NSString *source = @R"msl(
      using namespace metal;

      struct Vertex {
        float4 position [[position]];
        float2 texCoord [[attribute(0)]];
      };

      vertex Vertex vertex_shader(uint v_id [[vertex_id]]) {
        Vertex vtx;

        vtx.position.x = float(v_id & 1) * 4.0 - 1.0;
        vtx.position.y = float(v_id >> 1) * 4.0 - 1.0;
        vtx.position.z = 0.0;
        vtx.position.w = 1.0;

        vtx.texCoord = vtx.position.xy * 0.5 + 0.5;

        return vtx;
      }

      constexpr sampler s {};

      fragment float4 fragment_shader(Vertex v [[stage_in]],
                      texture2d<float> t [[texture(0)]]) {
        return t.sample(s, v.texCoord);
      }

      )msl";

      MTLCompileOptions *options = [[[MTLCompileOptions alloc] init] autorelease];
      options.languageVersion = MTLLanguageVersion1_1;

      NSError *error = nil;
      id<MTLLibrary> library = [device newLibraryWithSource:source options:options error:&error];
      if (error) {
        fprintf(stderr, "ERROR: newLibraryWithSource:options:error: failed!\n");
        abort();
      }

      // Create a render pipeline for blit operation
      MTLRenderPipelineDescriptor *desc = [[[MTLRenderPipelineDescriptor alloc] init] autorelease];

      desc.fragmentFunction = [library newFunctionWithName:@"fragment_shader"];
      desc.vertexFunction = [library newFunctionWithName:@"vertex_shader"];

      [desc.colorAttachments objectAtIndexedSubscript:0].pixelFormat = framebufferMetalPixelFormat;

      m_metalRenderPipeline = (MTLRenderPipelineState *)[device
          newRenderPipelineStateWithDescriptor:desc
                                         error:&error];
      if (error) {
        fprintf(stderr, "ERROR: newRenderPipelineStateWithDescriptor:error: failed!\n");
        abort();
      }
    }
  }
}

GHOST_ContextCGL::~GHOST_ContextCGL()
{
  if (m_metalCmdQueue) {
    [m_metalCmdQueue release];
  }
  if (m_metalRenderPipeline) {
    [m_metalRenderPipeline release];
  }
  if (m_defaultFramebufferMetalTexture) {
    [m_defaultFramebufferMetalTexture release];
  }

  if (m_openGLContext != nil) {
    if (m_openGLContext == [NSOpenGLContext currentContext]) {
      [NSOpenGLContext clearCurrentContext];
    }

    if (m_openGLContext != s_sharedOpenGLContext || s_sharedCount == 1) {
      assert(s_sharedCount > 0);

      s_sharedCount--;

      if (s_sharedCount == 0)
        s_sharedOpenGLContext = nil;

      [m_openGLContext release];
    }
  }
}

GHOST_TSuccess GHOST_ContextCGL::swapBuffers()
{
  if (m_openGLContext != nil) {
    assert(m_metalLayer);

    @autoreleasepool {
      updateDrawingContext();
      glFlush();

      assert(m_defaultFramebufferMetalTexture);

      id<CAMetalDrawable> drawable = [m_metalLayer nextDrawable];
      if (!drawable) {
        return GHOST_kFailure;
      }

      id<MTLCommandBuffer> cmdBuffer = [m_metalCmdQueue commandBuffer];

      MTLRenderPassDescriptor *passDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
      {
        auto attachment = [passDescriptor.colorAttachments objectAtIndexedSubscript:0];
        attachment.texture = drawable.texture;
        attachment.loadAction = MTLLoadActionDontCare;
        attachment.storeAction = MTLStoreActionStore;
      }

      id<MTLTexture> srcTexture = (id<MTLTexture>)m_defaultFramebufferMetalTexture;

      {
        id<MTLRenderCommandEncoder> enc = [cmdBuffer
            renderCommandEncoderWithDescriptor:passDescriptor];

        [enc setRenderPipelineState:(id<MTLRenderPipelineState>)m_metalRenderPipeline];
        [enc setFragmentTexture:srcTexture atIndex:0];
        [enc drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:3];

        [enc endEncoding];
      }

      [cmdBuffer presentDrawable:drawable];

      [cmdBuffer commit];
    }
    return GHOST_kSuccess;
  }
  else {
    return GHOST_kFailure;
  }
}

GHOST_TSuccess GHOST_ContextCGL::setSwapInterval(int interval)
{
  if (m_openGLContext != nil) {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [m_openGLContext setValues:&interval forParameter:NSOpenGLCPSwapInterval];
    [pool drain];
    return GHOST_kSuccess;
  }
  else {
    return GHOST_kFailure;
  }
}

GHOST_TSuccess GHOST_ContextCGL::getSwapInterval(int &intervalOut)
{
  if (m_openGLContext != nil) {
    GLint interval;

    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    [m_openGLContext getValues:&interval forParameter:NSOpenGLCPSwapInterval];

    [pool drain];

    intervalOut = static_cast<int>(interval);

    return GHOST_kSuccess;
  }
  else {
    return GHOST_kFailure;
  }
}

GHOST_TSuccess GHOST_ContextCGL::activateDrawingContext()
{
  if (m_openGLContext != nil) {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [m_openGLContext makeCurrentContext];
    [pool drain];
    return GHOST_kSuccess;
  }
  else {
    return GHOST_kFailure;
  }
}

GHOST_TSuccess GHOST_ContextCGL::releaseDrawingContext()
{
  if (m_openGLContext != nil) {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [NSOpenGLContext clearCurrentContext];
    [pool drain];
    return GHOST_kSuccess;
  }
  else {
    return GHOST_kFailure;
  }
}

unsigned int GHOST_ContextCGL::getDefaultFramebuffer()
{
  return m_defaultFramebuffer;
}

GHOST_TSuccess GHOST_ContextCGL::updateDrawingContext()
{
  if (m_openGLContext != nil) {
    if (m_metalLayer) {
      assert(m_defaultFramebuffer);

      NSRect bounds = [m_view bounds];
      NSSize backingSize = [m_view convertSizeToBacking:bounds.size];

      size_t width = (size_t)backingSize.width;
      size_t height = (size_t)backingSize.height;

      id<MTLTexture> tex = (id<MTLTexture>)m_defaultFramebufferMetalTexture;

      if (!tex || tex.width != width || tex.height != height) {
        return updateDefaultFramebuffer(width, height);
      }
    }

    return GHOST_kSuccess;
  }
  else {
    return GHOST_kFailure;
  }
}

GHOST_TSuccess GHOST_ContextCGL::updateDefaultFramebuffer(size_t width, size_t height)
{
  CVReturn cvret;

  activateDrawingContext();

  NSDictionary *cvPixelBufferProps = @{
    (__bridge NSString *)kCVPixelBufferOpenGLCompatibilityKey : @YES,
    (__bridge NSString *)kCVPixelBufferMetalCompatibilityKey : @YES,
  };
  CVPixelBufferRef cvPixelBuffer = nil;
  cvret = CVPixelBufferCreate(kCFAllocatorDefault,
                              width,
                              height,
                              cvPixelFormat,
                              (__bridge CFDictionaryRef)cvPixelBufferProps,
                              &cvPixelBuffer);
  if (cvret != kCVReturnSuccess) {
    fprintf(stderr, "ERROR: CVPixelBufferCreate failed!\n");
    abort();
  }

  // Create an OpenGL texture
  CVOpenGLTextureCacheRef cvGLTexCache = nil;
  cvret = CVOpenGLTextureCacheCreate(kCFAllocatorDefault,
                                     nil,
                                     m_openGLContext.CGLContextObj,
                                     m_openGLContext.pixelFormat.CGLPixelFormatObj,
                                     nil,
                                     &cvGLTexCache);
  if (cvret != kCVReturnSuccess) {
    fprintf(stderr, "ERROR: CVOpenGLTextureCacheCreate failed!\n");
    abort();
  }

  CVOpenGLTextureRef cvGLTex = nil;
  cvret = CVOpenGLTextureCacheCreateTextureFromImage(
      kCFAllocatorDefault, cvGLTexCache, cvPixelBuffer, nil, &cvGLTex);
  if (cvret != kCVReturnSuccess) {
    fprintf(stderr, "ERROR: CVOpenGLTextureCacheCreateTextureFromImage failed!\n");
    abort();
  }

  unsigned int glTex;
  glTex = CVOpenGLTextureGetName(cvGLTex);

  // Create a Metal texture
  CVMetalTextureCacheRef cvMetalTexCache = nil;
  cvret = CVMetalTextureCacheCreate(
      kCFAllocatorDefault, nil, m_metalLayer.device, nil, &cvMetalTexCache);
  if (cvret != kCVReturnSuccess) {
    fprintf(stderr, "ERROR: CVMetalTextureCacheCreate failed!\n");
    abort();
  }

  CVMetalTextureRef cvMetalTex = nil;
  cvret = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                    cvMetalTexCache,
                                                    cvPixelBuffer,
                                                    nil,
                                                    framebufferMetalPixelFormat,
                                                    width,
                                                    height,
                                                    0,
                                                    &cvMetalTex);
  if (cvret != kCVReturnSuccess) {
    fprintf(stderr, "ERROR: CVMetalTextureCacheCreateTextureFromImage failed!\n");
    abort();
  }

  MTLTexture *tex = (MTLTexture *)CVMetalTextureGetTexture(cvMetalTex);

  if (!tex) {
    fprintf(stderr, "ERROR: CVMetalTextureGetTexture failed!\n");
    abort();
  }

  [m_defaultFramebufferMetalTexture release];
  m_defaultFramebufferMetalTexture = [tex retain];

  glBindFramebuffer(GL_FRAMEBUFFER, m_defaultFramebuffer);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, glTex, 0);

  [m_metalLayer setDrawableSize:CGSizeMake((CGFloat)width, (CGFloat)height)];

  CVPixelBufferRelease(cvPixelBuffer);
  CVOpenGLTextureCacheRelease(cvGLTexCache);
  CVOpenGLTextureRelease(cvGLTex);
  CFRelease(cvMetalTexCache);
  CFRelease(cvMetalTex);

  return GHOST_kSuccess;
}

static void makeAttribList(std::vector<NSOpenGLPixelFormatAttribute> &attribs,
                           bool coreProfile,
                           bool stereoVisual,
                           bool needAlpha,
                           bool softwareGL)
{
  attribs.clear();

  attribs.push_back(NSOpenGLPFAOpenGLProfile);
  attribs.push_back(coreProfile ? NSOpenGLProfileVersion3_2Core : NSOpenGLProfileVersionLegacy);

  // Pixel Format Attributes for the windowed NSOpenGLContext
  attribs.push_back(NSOpenGLPFADoubleBuffer);

  if (softwareGL) {
    attribs.push_back(NSOpenGLPFARendererID);
    attribs.push_back(kCGLRendererGenericFloatID);
  }
  else {
    attribs.push_back(NSOpenGLPFAAccelerated);
    attribs.push_back(NSOpenGLPFANoRecovery);
  }

  attribs.push_back(NSOpenGLPFAAllowOfflineRenderers);  // for automatic GPU switching

  if (stereoVisual)
    attribs.push_back(NSOpenGLPFAStereo);

  if (needAlpha) {
    attribs.push_back(NSOpenGLPFAAlphaSize);
    attribs.push_back((NSOpenGLPixelFormatAttribute)8);
  }

  attribs.push_back((NSOpenGLPixelFormatAttribute)0);
}

// TODO(merwin): make this available to all platforms
static void getVersion(int *major, int *minor)
{
#if 1  // legacy GL
  sscanf((const char *)glGetString(GL_VERSION), "%d.%d", major, minor);
#else  // 3.0+
  glGetIntegerv(GL_MAJOR_VERSION, major);
  glGetIntegerv(GL_MINOR_VERSION, minor);
#endif
}

GHOST_TSuccess GHOST_ContextCGL::initializeDrawingContext()
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  std::vector<NSOpenGLPixelFormatAttribute> attribs;
  attribs.reserve(40);

#ifdef GHOST_OPENGL_ALPHA
  static const bool needAlpha = true;
#else
  static const bool needAlpha = false;
#endif

  static bool softwareGL = getenv("BLENDER_SOFTWAREGL");  // command-line argument would be better
  GLint major = 0, minor = 0;
  NSOpenGLPixelFormat *pixelFormat;
  // TODO: keep pixel format for subsequent windows/contexts instead of recreating each time

  makeAttribList(attribs, m_coreProfile, m_stereoVisual, needAlpha, softwareGL);

  pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:&attribs[0]];

  if (pixelFormat == nil)
    goto error;

  m_openGLContext = [[NSOpenGLContext alloc] initWithFormat:pixelFormat
                                               shareContext:s_sharedOpenGLContext];
  [pixelFormat release];

  [m_openGLContext makeCurrentContext];

  getVersion(&major, &minor);
  if (m_debug) {
    fprintf(stderr, "OpenGL version %d.%d%s\n", major, minor, softwareGL ? " (software)" : "");
    fprintf(stderr, "Renderer: %s\n", glGetString(GL_RENDERER));
  }

  if (major < 2 || (major == 2 && minor < 1)) {
    // fall back to software renderer if GL < 2.1
    fprintf(stderr, "OpenGL 2.1 is not supported on your hardware, falling back to software");
    softwareGL = true;

    // discard hardware GL context
    [NSOpenGLContext clearCurrentContext];
    [m_openGLContext release];

    // create software GL context
    makeAttribList(attribs, m_coreProfile, m_stereoVisual, needAlpha, softwareGL);
    pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:&attribs[0]];
    m_openGLContext = [[NSOpenGLContext alloc] initWithFormat:pixelFormat
                                                 shareContext:s_sharedOpenGLContext];
    [pixelFormat release];

    [m_openGLContext makeCurrentContext];

    getVersion(&major, &minor);
    if (m_debug) {
      fprintf(stderr, "OpenGL version %d.%d%s\n", major, minor, softwareGL ? " (software)" : "");
      fprintf(stderr, "Renderer: %s\n", glGetString(GL_RENDERER));
    }
  }

#ifdef GHOST_MULTITHREADED_OPENGL
  // Switch openGL to multhreaded mode
  if (CGLEnable(CGLGetCurrentContext(), kCGLCEMPEngine) == kCGLNoError)
    if (m_debug)
      fprintf(stderr, "\nSwitched OpenGL to multithreaded mode\n");
#endif

#ifdef GHOST_WAIT_FOR_VSYNC
  {
    GLint swapInt = 1;
    /* wait for vsync, to avoid tearing artifacts */
    [m_openGLContext setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
  }
#endif

  initContextGLEW();

  if (m_metalLayer && m_defaultFramebuffer == 0) {
    // Create a virtual framebuffer
    [m_openGLContext makeCurrentContext];
    glGenFramebuffers(1, &m_defaultFramebuffer);

    updateDrawingContext();

    glBindFramebuffer(GL_FRAMEBUFFER, m_defaultFramebuffer);
  }

  if (s_sharedCount == 0)
    s_sharedOpenGLContext = m_openGLContext;

  s_sharedCount++;

  initClearGL();
  [m_openGLContext flushBuffer];

  [pool drain];

  return GHOST_kSuccess;

error:

  [pixelFormat release];

  [pool drain];

  return GHOST_kFailure;
}

GHOST_TSuccess GHOST_ContextCGL::releaseNativeHandles()
{
  m_openGLContext = NULL;

  return GHOST_kSuccess;
}
